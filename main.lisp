﻿;; Linear Congruential Pseudo Random Number Generator
(defun random-number (seed modulus multiplier increment)
  (abs (mod (+ (* seed multiplier) increment) modulus)))

;;these numbers are taken from the generator used in Java.util.random
;;they define a full period of (2^48 - 1) random numbers
(setf multiplier 25214903917)
(setf increment 11)
(setf modulus 281474976710655)

(defun generate-randoms (count seed)
  (cond
    ((= count 1)  (random-number seed modulus multiplier increment))
    ((= count 2) (let ((current-iteration (random-number seed modulus multiplier increment)))
		   (list current-iteration
			 (generate-randoms (- count 1) current-iteration))))
    (T (let ((current-iteration (random-number seed modulus multiplier increment)))
	 (cons current-iteration
	       (generate-randoms (- count 1) current-iteration))))))
