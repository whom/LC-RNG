(defpackage random-numbers
  (:use :common-lisp :asdf))

(in-package :random-numbers)

(defsystem "random-numbers"
  :description "random-numbers: a linear congruential random number generator"
  :version "0.1"
  :author "Aidan Hahn <aidanjacobhahn@gmail.com>"
  :licence "Public Domain"
  :components ((:file "main")
	       (:file "tests" :depends-on ("main")))
  :output-files (compile-op (o c) (list "main")))
