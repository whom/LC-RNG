# Linear Congruential Random Number Generator
A Linear Congruential Random Number Generator uses linear equations to create strings of pseudorandom numbers.

## How does that work?
A modulus is chosen, creating a ring of numbers or a set of equivalence classes.

Then a multiplier is chosen (the m in 'y = mx + b') and an increment is chosen (the b in 'y = mx + b')

In the equation y = mx + b,  x is the only parameter and y is the only output.

A seed value is passed into x for the first calculation, proceeding calculations will pass in the previous output as the parameter.

The output of each calculation will be reduced with a modulo operation into one of the equivalence classes defined by the modulus.

## Where'd you get your numbers?
These numbers were used by Java.util.random

The multiplier, increment, and modulus are very important numbers that take a good bit of research to come up with.

For this program I decided to use known good ones.